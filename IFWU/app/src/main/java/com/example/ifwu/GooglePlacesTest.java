package com.example.ifwu;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.ifwu.async.RetrieveAutocompleteCities;
import com.example.ifwu.interfaces.RetrieveAutocompleteCitiesListener;
import com.example.ifwu.pojo.City;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.location.places.Places;

import java.util.List;

// NOTE: This is not used anymore. Will probably be deleted in a future update.

public class GooglePlacesTest extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, RetrieveAutocompleteCitiesListener {

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_places_test);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        final GooglePlacesTest self = this;
        final TextView query = (TextView) this.findViewById(R.id.editText);

        Button searchButton = (Button) this.findViewById(R.id.complete_city_button);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RetrieveAutocompleteCities completeTask = new RetrieveAutocompleteCities(self, mGoogleApiClient);
                completeTask.execute(new RetrieveAutocompleteCities.Params(query.getText().toString(), true));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("google_places", "connected!");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("google_places", "connection suspended!");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("google_places", "connection failed!");
    }

    @Override
    public void onPossibleCityCompletionRetrieved(List<City> possibleCompletion) {
        if (possibleCompletion != null) {
            Log.d("google_places", "possible cities:");
            for (City city : possibleCompletion) {
                Log.d("google_places", String.format("%s, %s", city.getName(), city.getCountry()));
            }
        }
        else {
            Log.e("google_places", "could not find any city!");
        }
    }
}
