package com.example.ifwu;

import android.app.Application;
import android.content.Context;

/**
 * Created by francois on 16/11/16.
 */
public class IFWUApplication extends Application {
    private static Context sContext;

    public void onCreate(){
        super.onCreate();

        // Keep a reference to the application context
        sContext = getApplicationContext();
    }

    // Used to access Context anywhere within the app
    public static Context getContext() {
        return sContext;
    }
}
