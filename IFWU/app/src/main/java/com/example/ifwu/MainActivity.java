package com.example.ifwu;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ifwu.adapters.FollowingWeathersAdapter;
import com.example.ifwu.async.RetrieveWeatherDataAsyncTask;
import com.example.ifwu.helpers.WeatherHelper;
import com.example.ifwu.interfaces.WeatherDataChangeListener;
import com.example.ifwu.pojo.City;
import com.example.ifwu.pojo.WeatherData;
import com.example.ifwu.pojo.WeatherElement;
import com.example.ifwu.utils.Constants;
import com.example.ifwu.utils.PreferenceUtils;
import com.example.ifwu.utils.Utils;
import com.google.android.gms.location.places.AutocompleteFilter;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements WeatherDataChangeListener {
    private class RequestCode {
        public static final int SEARCH = 1;
        public static final int SETTINGS = 2;
    }

    private FollowingWeathersAdapter adapter;
    private Timer automaticUpdate;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.d("main", "unit:" + PreferenceUtils.getUnits());

        this.progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        this.startAutomaticUpdate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id) {
            case (R.id.action_settings) : {
                final Intent intent = new Intent(this, SettingsActivity.class);
                startActivityForResult(intent, RequestCode.SETTINGS);

                return true;
            }
            case (R.id.action_search) : {
                final Intent intent = new Intent(this, SearchActivity.class);
                startActivityForResult(intent, RequestCode.SEARCH);

                return true;
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private void updateCurrentWeatherUnits() {
        String newUnits = PreferenceUtils.getUnits();

        try {
            TextView temperature = (TextView) findViewById(R.id.TemperatureValue);
            String formattedTemp = temperature.getText().toString();

            float convertedValue = Utils.convertTemperatureToUnit(formattedTemp, newUnits);
            temperature.setText(String.format("%.01f°", convertedValue) + Utils.getUnitsSymbol(newUnits));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWeatherDataRetrieved(WeatherData weatherdata) {
        this.progressBar.setVisibility(View.GONE);

        if (weatherdata == null)
        {
            Toast.makeText(getApplicationContext(), "Unable to retrieve data, try again.", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            TextView cityName = (TextView) findViewById(R.id.CityName);
            TextView temperature = (TextView) findViewById(R.id.TemperatureValue);
            TextView windVelocity = (TextView) findViewById(R.id.WindVelocity);
            ImageView icon = (ImageView) findViewById(R.id.weather_icon);

            WeatherElement current = weatherdata.getContent().get(0);

            City city = weatherdata.getCity();
            cityName.setText(String.format("%s (%s)", city.getName(), city.getCountry()));

            temperature.setText(String.format("%.01f°", current.getTemp()) + Utils.getUnitsSymbol(PreferenceUtils.getUnits()));
            windVelocity.setText(String.format("%.01f km/h", current.getSpeed()));

            int iconId = Utils.getWeatherIcon(current.getMain());

            try {
                icon.setImageResource(iconId);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            adapter = new FollowingWeathersAdapter(getApplicationContext(), weatherdata.getContent().subList(1, weatherdata.getContent().size()),
                    PreferenceUtils.getUnits());
            ListView followingWeather = (ListView) findViewById(R.id.following_weather);
            followingWeather.setAdapter(adapter);

            Utils.setListViewHeightBasedOnChildren(followingWeather);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case (RequestCode.SEARCH): {
                if (resultCode == RESULT_OK) {
                    String cityName = data.getStringExtra("city_name");
                    String country = data.getStringExtra("country");

                    PreferenceUtils.setCity(cityName);
                    PreferenceUtils.setCountry(country);

                    this.startAutomaticUpdate();
                }
                else {
                    Log.e("main", "search activity returned failure.");
                }

                break;
            }
            case (RequestCode.SETTINGS) : {
                if (resultCode == RESULT_OK) {
                    try {
                        if (data.getBooleanExtra("changed_units", false)) {
                            Log.d("main", "changed units.");
                            this.updateCurrentWeatherUnits();
                            adapter.notifyDataSetChanged();
                        }

                        if (data.getBooleanExtra("changed_update_delay", false)) {
                            Log.d("main", "changed update delay.");
                            this.startAutomaticUpdate();
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                else {
                    Log.e("main", "settings activity returned failure.");
                }
                break;
            }
            default:
                break;
        }
    }

    public void startAutomaticUpdate() {
        final MainActivity self = this;

        if (this.automaticUpdate != null) {
            this.automaticUpdate.cancel();
        }

        this.automaticUpdate = new Timer();
        this.automaticUpdate.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                self.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    progressBar.setVisibility(View.VISIBLE);
                    }
                });

                RetrieveWeatherDataAsyncTask task = new RetrieveWeatherDataAsyncTask(self);
                task.execute(new RetrieveWeatherDataAsyncTask.Params(PreferenceUtils.getCity(), PreferenceUtils.getCountry(), PreferenceUtils.getUnits()));
            }
        }, 0, PreferenceUtils.getUpdatedelay() * 60 * 1000);

        Log.d("main", "automatic update every " + PreferenceUtils.getUpdatedelay() + " minutes.");
    }
}
