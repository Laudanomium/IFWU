package com.example.ifwu;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.ifwu.adapters.SearchedCitiesAdapter;
import com.example.ifwu.async.RetrieveAutocompleteCities;
import com.example.ifwu.database.DataBaseManager;
import com.example.ifwu.interfaces.CitySelectionListener;
import com.example.ifwu.pojo.City;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.server.converter.StringToIntConverter;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

public class SearchActivity extends AppCompatActivity implements CitySelectionListener {
    private AutocompleteFilter mAutocompleteFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setupActionBar();


        SearchedCitiesAdapter adapter = new SearchedCitiesAdapter(IFWUApplication.getContext(), DataBaseManager.getCities(), this);
        ListView searchedCitiesView = (ListView) findViewById(R.id.search_cities);
        searchedCitiesView.setAdapter(adapter);

        for (City city : DataBaseManager.getCities()) {
            Log.d("search", city.getName());
        }


        PlaceAutocompleteFragment autocompleteFragment =(PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setFilter(mAutocompleteFilter);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                String cityName = place.getName().toString();
                String address = place.getAddress().toString();

                try {
                    String country = address.split(",")[1];
                    selectCity(cityName, country.substring(1));
                }
                catch (Exception e) {
                    selectCity(cityName, null);
                }

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("search", "An error occurred: " + status);
            }
        });

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void selectCity(String city, String country) {
        Intent intent = new Intent();

        intent.putExtra("city_name", city);
        intent.putExtra("country", country);

        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onCitySelected(String city, String country) {
        this.selectCity(city, country);
    }
}
