package com.example.ifwu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.ifwu.utils.Constants;
import com.example.ifwu.utils.PreferenceUtils;

// TODO: Add ListView with already searched cities.
public class SettingsActivity extends AppCompatActivity {
    private final String[] possibleUnits = new String[]{"Celsius", "Fahrenheit", "Kelvin"};
    private final String[] possibleUpdateDelays = new String[]{"10 minutes", "1 hour", "3 hours", "6 hours", "24 hours"};

    private String oldUnits;
    private int oldUpdateDelay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        setContentView(R.layout.activity_settings);

        this.oldUnits = PreferenceUtils.getUnits();
        this.oldUpdateDelay = PreferenceUtils.getUpdatedelay();

        Spinner spinner = (Spinner)findViewById(R.id.unit_spinner);
        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, this.possibleUnits);
        spinner.setAdapter(adapter);

        int selection;

        if (PreferenceUtils.getUnits() == null) {
            selection = 2;
        }
        else {
            selection = (PreferenceUtils.getUnits().equals(Constants.OpenWeatherMap.IN_CELSIUS)) ? 0 : 1;
        }

        spinner.setSelection(selection);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case (0):
                    {
                        PreferenceUtils.setUnits(Constants.OpenWeatherMap.IN_CELSIUS);
                        break;
                    }
                    case (1): {
                        PreferenceUtils.setUnits(Constants.OpenWeatherMap.IN_FAHRENHEIT);
                        break;
                    }
                    default:
                    {
                        // TODO : Use this instead of null when converting.
                        //PreferenceUtils.setUnits(Constants.OpenWeatherMap.IN_KELVIN);
                        PreferenceUtils.setUnits(null);
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner = (Spinner)findViewById(R.id.update_delay_spinner);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, this.possibleUpdateDelays);
        spinner.setAdapter(adapter);

        switch (PreferenceUtils.getUpdatedelay()) {
            case (10): {
                selection = 0;
                break;
            }
            case (1 * 60) : {
                selection = 1;
                break;
            }
            case (3 * 60) : {
                selection = 2;
                break;
            }
            case ( 6 * 60) : {
                selection = 3;
                break;
            }
            default: {
                selection = 4;
                break;
            }
        }
        spinner.setSelection(selection);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case (0):
                    {
                        PreferenceUtils.setUpdateDelay(10);
                        break;
                    }
                    case (1):
                    {
                        PreferenceUtils.setUpdateDelay(1 * 60);
                        break;
                    }
                    case (2):
                    {
                        PreferenceUtils.setUpdateDelay(3 * 60);
                        break;
                    }
                    case (3):
                    {
                        PreferenceUtils.setUpdateDelay(6 * 60);
                        break;
                    }
                    default:
                    {
                        PreferenceUtils.setUpdateDelay(24 * 60);
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent();

            intent.putExtra("changed_units", this.oldUnits != PreferenceUtils.getUnits());
            intent.putExtra("changed_update_delay", this.oldUpdateDelay != PreferenceUtils.getUpdatedelay());

            setResult(RESULT_OK, intent);
            finish();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
