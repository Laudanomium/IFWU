package com.example.ifwu.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ifwu.R;
import com.example.ifwu.pojo.WeatherElement;
import com.example.ifwu.utils.Constants;
import com.example.ifwu.utils.PreferenceUtils;
import com.example.ifwu.utils.Utils;



import java.util.List;

/**
 * Created by francois on 16/11/16.
 */
public class FollowingWeathersAdapter extends ArrayAdapter<WeatherElement> {
    // View lookup cache
    private static class ViewHolder {
        TextView time;
        TextView temp;
        ImageView icon;
    }

    private String units;

    public FollowingWeathersAdapter(Context context, List<WeatherElement> weathers, String units) {
        super(context, R.layout.following_weather_layout, weathers);
        this.units = units;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        WeatherElement element = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.following_weather_layout, parent, false);

            viewHolder.time = (TextView) convertView.findViewById(R.id.weather_time);
            viewHolder.time.setTextColor(Color.BLACK);
            viewHolder.temp = (TextView) convertView.findViewById(R.id.weather_temp);
            viewHolder.temp.setTextColor(Color.BLACK);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.weather_icon);
            viewHolder.time.setTextColor(Color.BLACK);
            viewHolder.temp.setTextColor(Color.BLACK);

            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);

        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.time.setText(element.getDt_txt());

        String currentUnits = PreferenceUtils.getUnits();
        String unitsSymol = Utils.getUnitsSymbol(currentUnits);

        viewHolder.temp.setText(String.format("%.01f°", Utils.convertTemperatureToUnit(units, currentUnits, element.getTemp())) + unitsSymol);

        int iconeInt = R.drawable.cloud; //default
        String icone = element.getIcon();
        switch (icone){
            case "01d":
                 iconeInt = R.drawable.sun;
                break;
            case "01n":
                 iconeInt = R.drawable.night;
                break;
            case "02d":
                 iconeInt = R.drawable.cloud;
                break;
            case "02n":
                iconeInt = R.drawable.night;
                break;
            case "03d":
                iconeInt = R.drawable.cloud;
                break;
            case "03n":
                iconeInt = R.drawable.cloud;
                break;
            case "04d":
                iconeInt = R.drawable.cloud;
                break;
            case "04n":
                iconeInt = R.drawable.cloud;
                break;
            case "09d":
                iconeInt = R.drawable.rain;
                break;
            case "09n":
                iconeInt = R.drawable.rain;
                break;
            case "10d":
                iconeInt = R.drawable.rain;
                break;
            case "10n":
                iconeInt = R.drawable.rain;
                break;
            case "11d":
                iconeInt = R.drawable.thunder;
                break;
            case "11n":
                iconeInt = R.drawable.thunder;
                break;
            case "13d":
                iconeInt = R.drawable.snow;
                break;
            case "13n":
                iconeInt = R.drawable.snow;
                break;
            case "50d":
                iconeInt = R.drawable.fog;
                break;
            case "50n":
                iconeInt = R.drawable.fog;
                break;
        }
        viewHolder.icon.setImageResource(iconeInt);

        // Return the completed view to render on screen
        return convertView;
    }
}