package com.example.ifwu.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ifwu.R;
import com.example.ifwu.interfaces.CitySelectionListener;
import com.example.ifwu.pojo.City;
import com.example.ifwu.pojo.WeatherElement;
import com.example.ifwu.utils.Constants;
import com.example.ifwu.utils.PreferenceUtils;
import com.example.ifwu.utils.Utils;



import java.util.List;

/**
 * Created by francois on 16/11/16.
 */
public class SearchedCitiesAdapter extends ArrayAdapter<City> {
    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView country;
    }

    private CitySelectionListener mListener;

    public SearchedCitiesAdapter(Context context, List<City> cities, CitySelectionListener listener) {
        super(context, R.layout.city_layout, cities);
        this.mListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final City element = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.city_layout, parent, false);

            try {
                viewHolder.name = (TextView) convertView.findViewById(R.id.searched_city_name);
                viewHolder.name.setTextColor(Color.BLACK);
                viewHolder.country = (TextView) convertView.findViewById(R.id.searched_city_country);

                viewHolder.name.setTextColor(Color.BLACK);
                viewHolder.country.setTextColor(Color.BLACK);
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);

        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }

        try {
            // Populate the data from the data object via the viewHolder object
            // into the template view.
            viewHolder.name.setText(element.getName());

            String currentUnits = PreferenceUtils.getUnits();
            String unitsSymol = Utils.getUnitsSymbol(currentUnits);

            viewHolder.country.setText("(" + element.getCountry().toUpperCase() + ")");

            final String cityName = element.getName();
            final String country = element.getCountry();

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onCitySelected(cityName, country);
                }
            });
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        // Return the completed view to render on screen
        return convertView;
    }
}