package com.example.ifwu.async;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;

import com.example.ifwu.database.DataBaseManager;
import com.example.ifwu.interfaces.RetrieveAutocompleteCitiesListener;
import com.example.ifwu.pojo.City;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by francois on 16/11/16.
 */
public class RetrieveAutocompleteCities extends AsyncTask<RetrieveAutocompleteCities.Params, Void, List<City>> {

    private RetrieveAutocompleteCitiesListener mListener;
    private GoogleApiClient mGoogleApiClient;
    private AutocompleteFilter mAutocompleteFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build();

    public RetrieveAutocompleteCities(RetrieveAutocompleteCitiesListener listener, GoogleApiClient client) {
        this.mListener = listener;
        this.mGoogleApiClient = client;
    }

    // TODO:DONE (Hugo).
    public List<City> completeCityQueryFromDatabase(String query) {
        List<City> cities = DataBaseManager.getCities();
        for (City city : cities) {
            if(!city.getName().contains(query) || !city.getCountry().contains(query)){
                cities.remove(city);
            }
        }
        return cities;
    }

    public List<City> completeCityQueryFromGoogle(String query) {
        if (mGoogleApiClient.isConnected()) {
            PendingResult<AutocompletePredictionBuffer> result = Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, query, null, mAutocompleteFilter);

            AutocompletePredictionBuffer autocompletePredictions = result.await(10, TimeUnit.SECONDS);

            if (!autocompletePredictions.getStatus().isSuccess()) {
                Log.e("google_places", "completion failed!");
                return null;
            }

            ArrayList<City> citylist = new ArrayList<>(autocompletePredictions.getCount());

            Log.d("google_places", String.format("found %d places.", autocompletePredictions.getCount()));
            final CharacterStyle style = new StyleSpan(Typeface.BOLD);

            for (int i = 0; i < autocompletePredictions.getCount(); ++i) {
                AutocompletePrediction prediction = autocompletePredictions.get(i);

                City city = new City();
                city.setName(prediction.getPrimaryText(style).toString());
                city.setCountry(prediction.getSecondaryText(style).toString());

                citylist.add(city);
            }

            return citylist;
        }

        Log.e("google_places", "google client is not  connected!");
        return null;
    }

    @Override
    protected List<City> doInBackground(Params... params) {
        if ((params != null) && (params.length > 0)) {
            if (params[0].fromGoogle) {
                return completeCityQueryFromGoogle(params[0].query);
            }

            return completeCityQueryFromDatabase(params[0].query);
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<City> result) {
        if (mListener != null){
            mListener.onPossibleCityCompletionRetrieved(result);
        }
    }

    public static class Params {
        public String query;
        public boolean fromGoogle;

        public Params(String query, boolean fromGoogle) {
            this.query = query;
            this.fromGoogle = fromGoogle;
        }
    }
}
