package com.example.ifwu.async;

import com.example.ifwu.helpers.WeatherHelper;
import com.example.ifwu.interfaces.WeatherDataChangeListener;
import com.example.ifwu.pojo.WeatherData;
import android.os.AsyncTask;

public class RetrieveWeatherDataAsyncTask extends AsyncTask<RetrieveWeatherDataAsyncTask.Params, Void, WeatherData>{

    // A reference to the listener
    private WeatherDataChangeListener mListener;

    public RetrieveWeatherDataAsyncTask(WeatherDataChangeListener listener){
        mListener = listener;
    }

    @Override
    protected WeatherData doInBackground(Params... params) {
        if ((params != null) && (params.length > 0)){
            try {
                Params p = params[0];

                return WeatherHelper.getWeatherFromCity(p.cityId, p.cityName, p.country, p.units);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(WeatherData result) {
        if (mListener != null){
            mListener.onWeatherDataRetrieved(result);
        }
    }

    public static class Params {
        int cityId = 0;
        String cityName = null;
        String country = null;
        String units = null;

        Params(int cityId, String cityName, String country, String units) {
            this.cityId = cityId;
            this.cityName = cityName;
            this.country = country;
            this.units = units;
        }

        public Params(int cityId, String units) {
            this(cityId, null, null, units);
        }

        public Params(String cityName, String country, String units) {
            this(0, cityName, country, units);
        }
    }
}