package com.example.ifwu.database;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by hugob on 16/11/2016.
 */
public class DataBaseContract implements BaseColumns {

    //Field names
    public static final String CITY_NAME = "cityName";
    public static final String COUNTRY_NAME = "countryName";
    public static final String FAVORITE_CITY = "favoriteCity";

        //Optionnal Weather
    public static final String TEMPERATURE = "temperature";
    public static final String LAST_UPDATE = "lastUpdate";
    public static final String WIND = "wind";
    public static final String WEATHER = "weather";

    /*
        //Optionnal Days to come
    public static final String NEXT_DAYS_NUMBER = "nextDaysNumber";
    public static final String DATE = "date";
    public static final String HOUR = "hour";
    */

    //Table name
    public static final String TABLE_CITIES = "cities";

    //Table scripts creation
    private static final String TABLE_GENERIC_CREATE_SCRIPT_PREFIX = "CREATE TABLE IF NOT EXISTS ";
    private static final String TABLE_IMAGES_CREATE_SCRIPT_SUFFIX = "(" +
            _ID + " INTEGER PRIMARY KEY, " +
            CITY_NAME + " TEXT NOT NULL, " +
            COUNTRY_NAME + " TEXT NOT NULL, " +
            FAVORITE_CITY + " INTEGER, " +
            LAST_UPDATE + " INTEGER, " +
            TEMPERATURE + " INTEGER," +
            WEATHER + " TEXT, " +
            WIND + " INTEGER" +
            ")";
            //FAVORITE_CITY + " BOOLEAN)";


    public static final String TABLE_CITIES_CREATE_SCRIPT =
            TABLE_GENERIC_CREATE_SCRIPT_PREFIX + TABLE_CITIES + TABLE_IMAGES_CREATE_SCRIPT_SUFFIX;


    // The projections
    public static final String[] PROJECTION_FULL = new String[]{
            _ID,
            CITY_NAME,
            COUNTRY_NAME,
            FAVORITE_CITY,
            LAST_UPDATE,
            TEMPERATURE,
            WEATHER,
            WIND
    };

    // Selections
    public static final String SELECTION_BY_ID = _ID + "=?";
    public static final String SELECTION_BY_CITY= CITY_NAME + "=?";

    // Sort order
    //public static final String ORDER_BY_DATE_CREATED_TIMESTAMP_DESCENDING = DATE_CREATED_TIMESTAMP + " DESC";

}
