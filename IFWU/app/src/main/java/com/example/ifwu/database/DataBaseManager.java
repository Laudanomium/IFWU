package com.example.ifwu.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.example.ifwu.IFWUApplication;
import com.example.ifwu.pojo.City;
import com.example.ifwu.pojo.WeatherElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by hugob on 16/11/2016.
 */
public class DataBaseManager {

    public static City cityFromCursor(Cursor c){
        if(null != c) {
            final City city = new City();
            if (c.getColumnIndex(DataBaseContract.CITY_NAME) >= 0) {
                city.setName(c.getString(c.getColumnIndex(DataBaseContract.CITY_NAME)));
            }
            if (c.getColumnIndex(DataBaseContract.COUNTRY_NAME) >= 0) {
                city.setCountry(c.getString(c.getColumnIndex(DataBaseContract.COUNTRY_NAME)));
            }
            if (c.getColumnIndex(DataBaseContract._ID) >= 0) {
                city.setId(c.getInt(c.getColumnIndex(DataBaseContract._ID)));
            }
            return city;
        }

        return null;
    }

    public static ContentValues cityToContentValues(City city){
        final ContentValues values = new ContentValues();

        if(!TextUtils.isEmpty(city.getName())){
            values.put(DataBaseContract.CITY_NAME, city.getName());
        }
        if(!TextUtils.isEmpty(city.getCountry())){
            values.put(DataBaseContract.COUNTRY_NAME, city.getCountry());
        }

        if(city.getId() != 0){
            values.put(DataBaseContract._ID, city.getId());
        }

        return values;
    }

    public static void isCityInDB(City city, WeatherElement weatherElement){
        final SQLiteOpenHelper sqLiteOpenHelper = new DataBaseHelper(IFWUApplication.getContext());
        final SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DataBaseContract.TABLE_CITIES +
                " WHERE " + DataBaseContract._ID +
                "=" + city.getId(), null);

        if(cursor == null ||cursor.getCount() < 1){
            DataBaseManager.addCity(city, weatherElement);
        }else{
            ContentValues values = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, values);
            //DataBaseManager.updateCityWeather(values, weatherElement);
        }
    }

    public static void addCity(City city, WeatherElement weatherElement){
        final SQLiteOpenHelper sqLiteOpenHelper = new DataBaseHelper(IFWUApplication.getContext());
        final SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();
        final ContentValues values = DataBaseManager.cityToContentValues(city);
        db.insert(DataBaseContract.TABLE_CITIES, "", values);

        //DataBaseManager.updateCityWeather(values, weatherElement);
    }


    public static void updateCityWeather(ContentValues values, WeatherElement weatherElement){
        final SQLiteOpenHelper sqLiteOpenHelper = new DataBaseHelper(IFWUApplication.getContext());
        final SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        db.update(DataBaseContract.TABLE_CITIES, values, DataBaseContract.TEMPERATURE + "=" + (weatherElement.getTemp()-260), null);
        db.update(DataBaseContract.TABLE_CITIES, values, DataBaseContract.FAVORITE_CITY + "=0", null);
        db.update(DataBaseContract.TABLE_CITIES, values, DataBaseContract.LAST_UPDATE + "=" + weatherElement.getDt(), null);
        db.update(DataBaseContract.TABLE_CITIES, values, DataBaseContract.WEATHER + "=" + weatherElement.getDescription(), null);
        db.update(DataBaseContract.TABLE_CITIES, values, DataBaseContract.WIND + "=" + (int)weatherElement.getSpeed(), null);
    }
    public static List<City> getCities(){
        final SQLiteOpenHelper sqLiteOpenHelper = new DataBaseHelper(IFWUApplication.getContext());
        final SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        final Cursor cursor = db.query(DataBaseContract.TABLE_CITIES, DataBaseContract.PROJECTION_FULL,
                null, null, null, null, null);

        List<City> DBCities = new ArrayList<City>();
        while(cursor.moveToNext()){
            final String cityName = cursor.getString(cursor.getColumnIndex(DataBaseContract.CITY_NAME));
            final String countryName = cursor.getString(cursor.getColumnIndex(DataBaseContract.COUNTRY_NAME));
            final String description = cursor.getString(cursor.getColumnIndex(DataBaseContract.WEATHER));
            final City city = new City();
            city.setName(cityName);
            city.setCountry(countryName);

            int index = -1;

            for (City savedCity:
                 DBCities) {
                if (savedCity.getName().equals(cityName) && savedCity.getCountry().equals(countryName)) {
                    index = DBCities.indexOf(savedCity);
                }
            }
            if (index != -1) {
                DBCities.remove(index);
            }

            DBCities.add(city);

            Log.i("QUERY ", cityName + " " + description);
        }

        if(!cursor.isClosed()){
            cursor.close();
        }

        if (DBCities.size() > 10) {
            DBCities = DBCities.subList(DBCities.size() - 10, DBCities.size());
        }
        Collections.reverse(DBCities);
        return DBCities;
    }

    public static void removeCity(City city){
        final SQLiteOpenHelper sqLiteOpenHelper = new DataBaseHelper(IFWUApplication.getContext());
        final SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

    }
}
