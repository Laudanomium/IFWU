package com.example.ifwu.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.ifwu.IFWUApplication;
import com.example.ifwu.WeatherTest;
import com.example.ifwu.database.DataBaseContract;
import com.example.ifwu.database.DataBaseHelper;
import com.example.ifwu.database.DataBaseManager;
import com.example.ifwu.pojo.City;
import com.example.ifwu.pojo.WeatherData;
import com.example.ifwu.pojo.WeatherElement;
import com.example.ifwu.utils.Constants;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;

public class WeatherHelper {

    // NOTE: None of this is used for now (and probably won't).
    // Proxy stuff
    private static final boolean USE_PROXY = false;
    private static final String PROXY_HOST = "prx-dev02.priv.atos.fr";
    private static final int PROXY_PORT = 3128;
    private static final boolean USE_PROXY_AUTHENTICATION = false;
    private static final String PROXY_USERNAME = "training10";
    private static final String PROXY_PASSWORD = "Student10/";

    // NOTE: May be useful later if we decide (for whatever reason) to download icons as well.
    /*public static Bitmap getTwitterUserImage(String imageUrl) throws Exception{
        final HttpURLConnection connection = getHTTPUrlConnection(imageUrl);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");

        final int responseCode = connection.getResponseCode();

        // If success
        if (responseCode == 200){
            final Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
            return bitmap;
        }
        return null;
    }*/

    // TODO: Read database first.
    public static WeatherData getWeatherFromCity(int cityId, String cityName, String country, String units) throws Exception{
        // Create the HTTP Get request to OpenWeatherMap servers


        String url = Constants.OpenWeatherMap.URL;

        url += (cityId != 0) ? "id=" + cityId : "q=" + cityName;
        url += ((cityId == 0) && (country != null)) ? "," + country : "";
        url += "&APPID=" + Constants.OpenWeatherMap.API_KEY;
        url += (units != null) ? "&units=" + units : "";
        Log.d("weather_helper", "url:" + url);

        final HttpURLConnection connection = getHTTPUrlConnection(url);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Type", "application/json");

        final int responseCode = connection.getResponseCode();

        // If success
        if (responseCode == 200){
            StringBuffer buffer = new StringBuffer();
            InputStream inputStream = connection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

            String line = null;
            while ((line = br.readLine()) != null)
                buffer.append(line + "rn");

            inputStream.close();
            connection.disconnect();

            if ((buffer.length() == 0) && (cityId == 0) && (country != null))
            {
                return WeatherHelper.getWeatherFromCity(cityId, cityName, null, units);
            }

            //IFWUApplication.getContext().deleteDatabase(DataBaseHelper.DATABASE_NAME);
            WeatherData weatherData = new WeatherData(buffer.toString());
            WeatherElement weatherElement = weatherData.getContent().get(0);
            City city = new City();
            city.setName(cityName);
            city.setCountry(country);
            city.setId(cityId);
            DataBaseManager.isCityInDB(city, weatherElement);
            DataBaseManager.getCities();
            return weatherData;
        }

        return null;
    }

    private static HttpURLConnection getHTTPUrlConnection(String url) throws Exception {
        if (USE_PROXY){
            final Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_HOST, PROXY_PORT));

            if (USE_PROXY_AUTHENTICATION){
                Authenticator authenticator = new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return (new PasswordAuthentication(PROXY_USERNAME, PROXY_PASSWORD.toCharArray()));
                    }
                };
                Authenticator.setDefault(authenticator);
            }

            return (HttpURLConnection) new URL(url).openConnection(proxy);
        } else {
            return (HttpURLConnection) new URL(url).openConnection();
        }
    }

    public static WeatherData getFakeWeatherData() {
        try {
            final WeatherData weatherData = new WeatherData(WeatherTest.apiRequestExample);
            return weatherData;
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}

