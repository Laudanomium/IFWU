package com.example.ifwu.interfaces;

/**
 * Created by francois on 18/11/16.
 */
public interface CitySelectionListener {
    public void onCitySelected(String city, String country);
}
