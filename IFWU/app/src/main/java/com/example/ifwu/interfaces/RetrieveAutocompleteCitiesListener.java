package com.example.ifwu.interfaces;

import com.example.ifwu.pojo.City;

import java.util.List;

/**
 * Created by francois on 16/11/16.
 */
public interface RetrieveAutocompleteCitiesListener {
    public void onPossibleCityCompletionRetrieved(List<City> possibleCompletion);
}
