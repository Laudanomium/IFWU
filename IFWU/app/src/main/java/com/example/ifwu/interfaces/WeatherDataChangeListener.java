package com.example.ifwu.interfaces;

import com.example.ifwu.pojo.WeatherData;

public interface WeatherDataChangeListener {
    public void onWeatherDataRetrieved(WeatherData weatherdata);
}
