package com.example.ifwu.pojo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by francois on 16/11/16.
 */
// TODO: Set as parceable (for storage in a database).
public class City {
    private int id;
    private String name, country;
    private float lon, lat;

    public City() {

    }

    public City(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.getInt("id");
        this.name = jsonObject.getString("name");
        this.country = jsonObject.getString("country");

        JSONObject jsonCoordinates = jsonObject.getJSONObject("coord");

        this.lon = (float) jsonCoordinates.getDouble("lon");
        this.lat = (float) jsonCoordinates.getDouble("lat");
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public int getId() {
        return id;
    }

    public float getLongitude() {
        return lon;
    }

    public float getLatitude() {
        return lat;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLongitude(float lon) {
        this.lon = lon;
    }

    public void setLatitude(float lat) {
        this.lat = lat;
    }
}
