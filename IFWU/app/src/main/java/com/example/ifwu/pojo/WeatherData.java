package com.example.ifwu.pojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by francois on 15/11/16.
 */
// TODO: Set as parceable (for storage in a database).
public class WeatherData {
    private City city;
    private List<WeatherElement> content = new ArrayList<>();

    public WeatherData(String jsonData) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonData);

        JSONObject jsonCity = jsonObject.getJSONObject("city");
        this.city = new City(jsonCity);

        JSONArray jsonList = jsonObject.getJSONArray("list");

        for (int i = 0; i < jsonList.length(); ++i)
        {
            this.content.add(new WeatherElement(jsonList.getJSONObject(i)));
        }
    }

    public WeatherData() {}

    public City getCity() {
        return city;
    }
    public List<WeatherElement> getContent() { return content; }
}
