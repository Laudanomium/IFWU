package com.example.ifwu.pojo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by francois on 16/11/16.
 */
// TODO: Set as parceable (for storage in a database).
//       See what fields we keep.
// NOTE: Some fields may not appear if they were not mesured.
//       Use a hash table instead?
public class WeatherElement {
    int dt;

    String dt_txt;
    float snow = -1, rain = -1, deg, speed;
    int clouds, humidity;
    String icon, main, description;
    float temp_kf, grnd_level, sea_level,
            pressure, temp_max, temp_min, temp;

    public WeatherElement(JSONObject jsonObject) throws JSONException {
        this.dt = jsonObject.getInt("dt");
        this.dt_txt = jsonObject.getString("dt_txt");

        try {
            JSONObject jsonSnow = jsonObject.getJSONObject("snow");
            this.snow = (float) jsonSnow.getDouble("3h");
        } catch (JSONException e) {
        }

        try {
            JSONObject jsonRain = jsonObject.getJSONObject("rain");
            this.rain = (float) jsonRain.getDouble("3h");
        } catch (JSONException e) {
        }

        try {
            JSONObject jsonWind = jsonObject.getJSONObject("wind");
            this.deg = (float) jsonWind.getDouble("deg");
            this.speed = (float) jsonWind.getDouble("speed");
        } catch (JSONException e) {
        }


        try {
            JSONObject jsonClouds = jsonObject.getJSONObject("clouds");
            this.clouds = jsonClouds.getInt("all");
        } catch (JSONException e) {
        }

        try {
            JSONObject jsonWeather = jsonObject.getJSONArray("weather").getJSONObject(0);
            this.icon = jsonWeather.getString("icon");
            this.main = jsonWeather.getString("main").toLowerCase();
            this.description = jsonWeather.getString("description");
        } catch (JSONException e) {
        }

        try {
            JSONObject jsonMain = jsonObject.getJSONObject("main");
            this.temp_kf = (float) jsonMain.getDouble("temp_kf");
            this.humidity = jsonMain.getInt("humidity");
            this.grnd_level = (float) jsonMain.getDouble("grnd_level");
            this.sea_level = (float) jsonMain.getDouble("sea_level");
            this.pressure = (float) jsonMain.getDouble("pressure");
            this.temp_min = (float) jsonMain.getDouble("temp_min");
            this.temp_max = (float) jsonMain.getDouble("temp_max");
            this.temp = (float) jsonMain.getDouble("temp");
        } catch (JSONException e) {
        }
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public int getDt() {
        return dt;
    }

    public float getSnow() {
        return snow;
    }

    public float getDeg() {
        return deg;
    }

    public float getSpeed() {
        return speed;
    }

    public int getClouds() {
        return clouds;
    }

    public int getHumidity() {
        return humidity;
    }

    public String getIcon() {
        return icon;
    }

    public String getDescription() {
        return description;
    }

    public float getTemp_kf() {
        return temp_kf;
    }

    public float getGrnd_level() {
        return grnd_level;
    }

    public float getSea_level() {
        return sea_level;
    }

    public float getPressure() {
        return pressure;
    }

    public float getTemp_max() {
        return temp_max;
    }

    public float getTemp_min() {
        return temp_min;
    }

    public float getTemp() {
        return temp;
    }

    public float getRain() {
        return rain;
    }

    public String getMain() {
        return main;
    }
}
