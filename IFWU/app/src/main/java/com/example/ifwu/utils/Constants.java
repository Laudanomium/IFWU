package com.example.ifwu.utils;

/**
 * Created by francois on 15/11/16.
 */
public class Constants {
    public class OpenWeatherMap {
        public static final String URL = "http://api.openweathermap.org/data/2.5/forecast/city?";
        public static final String API_KEY = "94d393fb513178f6135f3f1db0a5839a";
        public static final String IN_CELSIUS = "metric";
        public static final String IN_FAHRENHEIT = "imperial";
        public static final String IN_KELVIN = "scientific";
    }

    public class Preferences {
        public static final String SHARED_PREFERENCES_FILE_NAME = "ifwuSharedPrefs";
        public static final String PREF_UNIT = "prefUnit";

        // NOTE: In minutes.
        public static final String PREF_UPDATE_DELAY = "prefUpdateDelay";

        public static final String PREF_CITY = "prefCity";
        public static final String PREF_COUNTRY = "prefCountry";
    }

    public class Default {
        //public static final String UNIT = OpenWeatherMap.IN_CELSIUS;
        public static final int UPDATE_DELAY = 10;
        public static final String CITY = "Lille";
        public static final String COUNTRY = "FR";
    }
}
