package com.example.ifwu.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.ifwu.IFWUApplication;

/**
 * Created by francois on 16/11/16.
 */
// TODO: Find why setting does not work.
public class PreferenceUtils {

    @SuppressWarnings("unused")
    private static SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    private static SharedPreferences getSharedPreferences(){
        return IFWUApplication.getContext().getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    public static void setUnits(String unit){
        final SharedPreferences prefs = getSharedPreferences();
        prefs.edit().putString(Constants.Preferences.PREF_UNIT, unit).commit();
    }

    public static String getUnits() {
        final SharedPreferences prefs = getSharedPreferences();
        return prefs.getString(Constants.Preferences.PREF_UNIT, null);
    }

    public static void setUpdateDelay(int updateDelay) {
        final SharedPreferences prefs = getSharedPreferences();
        prefs.edit().putInt(Constants.Preferences.PREF_UPDATE_DELAY, updateDelay).commit();
    }
    public static int getUpdatedelay() {
        final SharedPreferences prefs = getSharedPreferences();
        return prefs.getInt(Constants.Preferences.PREF_UPDATE_DELAY, Constants.Default.UPDATE_DELAY);
    }

    public static void setCity(String city) {
        final SharedPreferences prefs = getSharedPreferences();
        prefs.edit().putString(Constants.Preferences.PREF_CITY, city).commit();
    }
    public static String getCity() {
        final SharedPreferences prefs = getSharedPreferences();
        return prefs.getString(Constants.Preferences.PREF_CITY, Constants.Default.CITY);
    }

    public static void setCountry(String country) {
        final SharedPreferences prefs = getSharedPreferences();
        prefs.edit().putString(Constants.Preferences.PREF_COUNTRY, country).commit();
    }
    public static String getCountry() {
        final SharedPreferences prefs = getSharedPreferences();
        return prefs.getString(Constants.Preferences.PREF_COUNTRY, Constants.Default.COUNTRY);
    }
}
