package com.example.ifwu.utils;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.ifwu.R;

/**
 * Created by francois on 16/11/16.
 */
public class Utils {
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static int getWeatherIcon(String type) {
        switch (type) {
            case ("clear") : {
                return R.drawable.sun;
            }
            case ("cloud") : {
                return R.drawable.cloud;
            }
            case ("rain") : {
                return R.drawable.rain;
            }
            case ("snow") : {
                return R.drawable.snow;
            }
            case ("fog") : {
                return R.drawable.fog;
            }
            case ("thunder") : {
                return R.drawable.thunder;
            }
            case ("night") : {
                return R.drawable.night;
            }
            default : {
                return R.drawable.cloud;
            }
        }
    }

    public static String getUnitsSymbol(String units) {
        if (units == null) {
            return "K";
        }

        return (units.equals(Constants.OpenWeatherMap.IN_CELSIUS)) ? "C" : "F";
    }

    public static float convertTemperatureToUnit(String unit1, String unit2, float temp) {
        return Utils.convertTemperatureToUnit(String.format("%.01f°", temp) + Utils.getUnitsSymbol(unit1), unit2);
    }

    public static String getTemperatureUnits(String formattedTemp) {
        char unitSymbol = formattedTemp.charAt(formattedTemp.length() - 1);

        switch(unitSymbol) {
            case ('C') : {
                return Constants.OpenWeatherMap.IN_CELSIUS;
            }
            case ('F') : {
                return Constants.OpenWeatherMap.IN_FAHRENHEIT;
            }
            default: {
                return null;
            }
        }
    }

    public static float convertTemperatureToUnit(String formatedTemp, String unit) {
        String tempUnit = getTemperatureUnits(formatedTemp);
        formatedTemp = formatedTemp.replace(",", ".");

        float value = Float.valueOf(formatedTemp.substring(0, formatedTemp.length() - 2));

        if (tempUnit == null) {
            if (unit == null) {
                return value;
            }
            return (unit.equals(Constants.OpenWeatherMap.IN_CELSIUS)) ? convertKtoC(value) : convertKtoF(value);
        }

        if (tempUnit.equals(unit)) {
            return value;
        }

        switch (tempUnit) {
            case (Constants.OpenWeatherMap.IN_CELSIUS) : {
                return (unit == null) ? convertCtoK(value) : convertCtoF(value);
            }
            default : {
                return (unit == null) ? convertFtoK(value) : convertFtoC(value);
            }
        }
    }

    public static float convertCtoF(float c) {
        return  c * 1.8f + 32;
    }

    public static float convertCtoK(float c) {
        return c + 273.15f;
    }

    public static float convertFtoC(float f) { return(f - 32.0f) / 1.8f; }

    public static float convertFtoK(float f) {
        return (f + 459.67f) / 1.8f;
    }

    public static float convertKtoC(float k) {
        return k - 273.15f;
    }

    public static float convertKtoF(float k) {
        return k * 1.8f - 459.67f;
    }
}
